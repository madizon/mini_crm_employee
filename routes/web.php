<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]); 

Route::get('/', function () {
    return view('auth.login');
})->middleware('guest');


Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/lang/change', 'LangController@change')->name('changeLang');
Route::get('/timezone/change', 'TimezoneController@change')->name('changeTimezone');

Route::get('/error', function () {
    return view('not-found');
});
