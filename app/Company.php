<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model 
{
    use SoftDeletes;

    protected $table = "companies"; 

    protected $fillable = [
        'name', 'email', 'website', 'logo', 'created_by_id', 'updated_by_id'
    ];

    public function employee()
    {
        return $this->hasOne('App\Employee');
    }
}
