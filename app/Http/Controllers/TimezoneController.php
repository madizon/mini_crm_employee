<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TimezoneController extends Controller
{
    public function change(Request $request)
    {
        session()->put('timezone', $request->timezone);
  
        return redirect()->back(); 
    }
}
