<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DB;
use Redirect;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {        
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        try 
        {
            $credentials = request(['email', 'password']);
   
            if (! $token = auth()->attempt($credentials)) 
                {
                    $errors = ['email' => 'These credentials do not match our records.'];
                    return redirect()->back()->withInput($request->all())->withErrors($errors); 
                }

            return $this->respondWithToken($token);
        } 
        catch (\Throwable $th)
        {
            Log::debug($th);
            $errors = ['db' => 'Internal Server Error'];
            return Redirect::back()->withErrors($errors);
        }
       
    }

    protected function respondWithToken($token)
    {
        setcookie('jwt_token', $token);
        return redirect('/dashboard');
    }

    public function logout()
    {
        try 
        {
            auth()->logout();
            return redirect('/');
        } 
        catch (\Throwable $th) 
        {
            Log::debug($th);
            $errors = ['db' => 'Internal Server Error'];
            return Redirect::back()->withErrors($errors);
        }
      
    }

}
