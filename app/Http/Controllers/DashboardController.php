<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Redirect,Log;
use Carbon\Carbon;
use App\Employee;
use App\Company;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request) 
    { 
        try 
        {
            if (count($request->all()) > 0) 
            {
                //request rows only or request by or value is null 
                if(isset($request->numrows) && (!isset($request->by) || is_null($request->by)) && (!isset($request->value) || is_null($request->value)))  
                {
                    $employees = Employee::latest()->paginate($request->numrows);            
                }
                //request filter dates    
                elseif(in_array($request->by, array("created_at","updated_at")))
                {
                    $date = explode(",",str_replace(" - ",",",$request->datefilter));
                    $startDate = date('Y-m-d H:i:s', strtotime($date[0]));
                    $endDate = date('Y-m-d H:i:s', strtotime($date[1]));
                                    
                    $fromDate = Carbon::parse($startDate, session()->get('timezone'))->setTimezone('UTC');
                    $toDate =  Carbon::parse($endDate, session()->get('timezone'))->setTimezone('UTC');
                             
                    $employees = Employee::whereBetween('created_at',[$fromDate,$toDate])
                                ->latest()->paginate($request->numrows); 
                }  
                //search by row, search by and value
                else
                {
                    $employees = Employee::where($request->by, 'like' ,"%{$request->value}%")
                        ->latest()->paginate($request->numrows);                       
                }   

                return view('dashboard',compact('employees','request'));        
            }
            else
            {
                $employees = Employee::latest()->paginate(10);
                return view('dashboard',compact('employees'));
            }      
                     
        } 
        catch (\Throwable $th) 
        {
            Log::debug($th);
            $errors = ['db' => 'Internal Server Error'];
            return Redirect::back()->withErrors($errors);
        }
    }
}

