<?php

namespace App\Http\Middleware;

use Closure;
use App\Company;

class CheckDomain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $company = Company::where('website',$_SERVER['HTTP_HOST'])->first();

        if(!is_null($company))
        {
            $values = [
                'app.name' => $company->name,
                'app.id' => $company->id
            ];
            config($values);
           
            if($request->path() == "error") 
            {  
                return redirect('/');
            }  
        }
        else
        {
            if($request->path() != "error")
            {
                return redirect('/error');
            }  
        }
        

        return $next($request);
    }
}
