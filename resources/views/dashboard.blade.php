@extends('layouts.app')

@section('content')
<div class="container-fluid">
<br>
<h5 class="mb-2">{{ __('label.employees') }}</h5>

    <form method="GET">
      <div class="form-inline">
        <label>{{ trans('label.rows') }} </label>&nbsp;
        <input type="number" class="form-control col-md-1 changePagination" name="numrows" value="{{ isset($request->numrows) ? $request->numrows : 10 }}" >
      </div>  
      
      <div class="form-inline">
           <label>{{ trans('label.search_by') }}  </label>&nbsp;
           <select class="form-control col-md-2" name="by" id="searchby">         
                <option style="display:none"></option>  
                <option value="first_name" {{ isset($request->by) ? ($request->by == 'first_name' ? 'selected' : '') : '' }} > {{ trans('label.first_name') }} </option>
                <option value="last_name" {{ isset($request->by) ? ($request->by == 'last_name' ? 'selected' : '') : '' }} > {{ trans('label.last_name') }}</option>
                <option value="email" {{ isset($request->by) ? ($request->by == 'email' ? 'selected' : '') : '' }} > {{ trans('label.email') }}</option>
                <option value="phone" {{ isset($request->by) ? ($request->by == 'phone' ? 'selected' : '') : '' }} >{{ trans('label.contact_no') }}</option>
                <option value="created_at" {{ isset($request->by) ? ($request->by == 'created_at' ? 'selected' : '') : '' }} >{{ trans('label.created_at') }}</option>
                <option value="updated_at" {{ isset($request->by) ? ($request->by == 'updated_at' ? 'selected' : '') : '' }} >{{ trans('label.updated_at') }}</option>
            </select>
            &nbsp;

            &nbsp;
           <input type="text" name="datefilter" class="form-control col-md-4" id="reservationtime" value="{{ isset($request->datefilter) ? $request->datefilter : '' }}" style="display:none;">

            &nbsp;
           <input type="text" name="value" class="form-control col-md-3" id="searchValue" value="{{ isset($request->value) ? $request->value : '' }}"> 
           
            &nbsp;
           <input type="submit" value="{{ trans('label.search') }} " class="btn btn-block btn-info col-md-1">
      </div>
    </form>

                <table class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>{{ trans('label.first_name') }}</th>
                    <th>{{ trans('label.last_name') }}</th>
                    <th>{{ trans('label.email') }}</th> 
                    <th>{{ trans('label.contact_no') }}</th>   
                    <th>{{ trans('label.created_at') }}</th> 
                    <th>{{ trans('label.updated_at') }}</th>             
                  </tr>
                  </thead>
                  <tbody>
                  @if(is_null($employees))
                    </tbody>                  
                    </table>
                  @else
                    @foreach($employees as $employee)
                    <tr>
                      <td>{{ $employee->first_name }}</td>
                      <td>{{ $employee->last_name }}</td>
                      <td>{{ $employee->email }}</td>
                      <td>{{ $employee->phone }}</td>
                      <td>{{ date('d-M-Y h:i:s A', strtotime($employee->created_at->setTimezone(session()->get('timezone')))) }}</td>
                      <td>{{ date('d-M-Y h:i:s A', strtotime($employee->updated_at->setTimezone(session()->get('timezone')))) }}</td>
                    </tr> 
                    @endforeach              
                  </tbody>                  
                </table>
                <div>{{ trans('label.showing') }} 
                 {{ ($employees->currentpage()-1) * $employees->perpage()+1}} to {{ $employees->currentpage() * $employees->perpage()}}
                   of  {{ $employees->total() }} {{ trans('label.entries') }} 
                </div>
                <div class="d-flex justify-content-center">
                  {!! $employees->appends(request()->query())->links() !!}    
                </div>
              @endif  
  
</div>
@endsection


