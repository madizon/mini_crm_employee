<script>
  $(function () {
    $("table").DataTable({
      "responsive": true,
      "autoWidth": false,
      "ordering": false,
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "bInfo": false,
    });

    //Date range picker with time picker  
  $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    });
  
  //select option onchange
    $('#searchby').on('change', function() {
      if(this.value == "created_at" || this.value == "updated_at")
      {
        $("#searchValue").hide();
        $("#reservationtime").show();
      }
      else 
      {
        $("#searchValue").show();
        $("#reservationtime").hide();     
      }
  });

  //check selected value if reloaded
  if($('#searchby').val() == "created_at" || $('#searchby').val() == "updated_at")
      {
        $("#searchValue").hide();
        $("#reservationtime").show();
      }
      else 
      {
        $("#searchValue").show();
        $("#reservationtime").hide();     
      }
  });

  

//Change Language
 var langUrl = "{{ route('changeLang') }}";

  $(".changeLang").change(function(){
      window.location.href = langUrl + "?lang="+ $(this).val();
  });

  //Change Timezone
 var timezoneUrl = "{{ route('changeTimezone') }}";

  $(".changeTimezone").change(function(){
      window.location.href = timezoneUrl + "?timezone="+ $(this).val();
  });


//Pagination
var urlpaginate = window.location.href;

$(".changePagination").focusout(function(){
  var search_url = new URL( window.location.href);
  search_url.searchParams.set('numrows', $(this).val());
  window.location.href = search_url;     
 });
</script>